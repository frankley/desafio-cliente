<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ClienteController extends Controller
{
    public function index(){
        $estados = DB::table('estados')->get();
        $categorias = DB::table('categorias')->get();
        return view('cliente.index', compact('estados', 'categorias'));
    }

    public function store(Request $request){

        $cliente_id = DB::table('clientes')->insertGetId([
            'nome' => $request->nome,
            'estado_id' => $request->estado,
            'categoria_id' => $request->categoria,
            'tp_cliente' => $request->tp_cliente,
            'cpf_cnpj' => $request->cpf_cnpj,
            'data' => $request->data,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        for ($i=0; $i < $request->count_telefone; $i++) { 
            DB::table('telefones')->insert([
                'cliente_id' => $cliente_id,
                'contato' => $request->telefone[$i],
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        return response()->json('Inserido com Sucesso');
    }

    public function clientes(){
        $clientes = DB::table('clientes as c')
                        ->join('estados as est', 'est.id', 'c.estado_id')
                        ->join('categorias as cat', 'cat.id', 'c.categoria_id')
                        ->select('c.id as cliente_id','c.nome as nome', 'c.cpf_cnpj as cpf_cnpj', 
                            'c.tp_cliente as tp_cliente', 'c.data as data',
                            'est.estado as estado', 'cat.nome as categoria'
                        );
        if(!empty($_GET)){
            $clientes->where(function ($clientes){
                if(isset($_GET['nome']))
                    $clientes->where('c.nome', 'like', "%{$_GET['nome']}%");
                if(isset($_GET['categoria']))
                    $clientes->where('c.categoria_id', "{$_GET['categoria']}");
                if(isset($_GET['estado']))
                    $clientes->where('c.estado_id', "{$_GET['estado']}");
            });
        }
    
        $clientes = $clientes->get();
 
        return view('cliente.include.cliente-datatables', compact('clientes'));
    }

    public function telefones(){
        $telefones = DB::table('telefones')->where('cliente_id', $_GET['cliente_id'])->get();
        return view('cliente.include.telefone', compact('telefones'));
    }

    public function delete(){
        DB::table('clientes')->where('id', $_GET['cliente_id'])->delete();
        return response()->json('Cliente excluido com sucesso!');
    }

    public function editar(){
        $estados = DB::table('estados')->get();
        $categorias = DB::table('categorias')->get();
        $cliente = DB::table('clientes as c')
                        ->join('estados as est', 'est.id', 'c.estado_id')
                        ->join('categorias as cat', 'cat.id', 'c.categoria_id')
                        ->select('c.id as cliente_id','c.nome as nome', 'c.cpf_cnpj as cpf_cnpj', 
                            'c.tp_cliente as tp_cliente', 'c.data as data',
                            'est.id as estado_id', 'cat.id as categoria_id'
                        )
                        ->where('c.id', "{$_GET['cliente_id']}")
                        ->first();

        $telefones = DB::table('telefones')->where('cliente_id', $_GET['cliente_id'])->get();
        $count_telefones = DB::table('telefones')
                                ->select(DB::raw('count(contato) as total'))
                                ->where('cliente_id', $_GET['cliente_id'])
                                ->first();

        return view('cliente.include.editar', compact('cliente', 'categorias', 'estados', 'telefones', 'count_telefones'));
    }

    public function update(Request $request){
        // dd($request->all());
        DB::table('clientes')
            ->where('id', $request->cliente_id)
            ->update([
                'nome' => $request->update_nome,
                'estado_id' => $request->update_estado,
                'categoria_id' => $request->update_categoria,
                'tp_cliente' => $request->update_tp_cliente,
                'cpf_cnpj' => $request->update_cpf_cnpj,
                'data' => $request->update_data,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        // exclui telefone e reatualiza
        DB::table('telefones')->where('cliente_id', $request->cliente_id)->delete();
        for ($i=0; $i < $request->update_count_telefone; $i++) { 
            DB::table('telefones')->insert([
                'cliente_id' => $request->cliente_id,
                'contato' => $request->update_telefone[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        }

        return response()->json('Atualizado com Sucesso');
    }
}
