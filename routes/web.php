<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('cliente');
});

Route::group(['prefix' => 'cliente'], function(){
    Route::get('/', [App\Http\Controllers\ClienteController::class, 'index']);
    Route::post('store', [App\Http\Controllers\ClienteController::class, 'store']);
    Route::get('clientes', [App\Http\Controllers\ClienteController::class, 'clientes']);
    Route::get('telefones', [App\Http\Controllers\ClienteController::class, 'telefones']);
    Route::get('delete', [App\Http\Controllers\ClienteController::class, 'delete']);
    Route::get('editar', [App\Http\Controllers\ClienteController::class, 'editar']);
    Route::post('atualizar', [App\Http\Controllers\ClienteController::class, 'update']);
});