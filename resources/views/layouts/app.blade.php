<!DOCTYPE html>

<html lang="pt-br" class="default-style">

<head>
  <title>Desafio</title>

  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="IE=edge,chrome=1">
  <meta name="description" content="">
  <meta name="viewport"
    content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">

  {{-- favicon --}}
  <link rel="apple-touch-icon" sizes="180x180" href="{{asset('assets/img/favicon/apple-touch-icon.png')}}">
  <link rel="icon" type="image/png" sizes="32x32" href="{{asset('assets/img/favicon/favicon-32x32.png')}}">
  <link rel="icon" type="image/png" sizes="16x16" href="{{asset('assets/img/favicon//favicon-16x16.png')}}">
  <link rel="manifest" href="{{asset('assets/img/favicon//site.webmanifest')}}">
  <link rel="icon" type="image/x-icon" href="{{asset('assets/img/favicon/favicon.ico')}}">

  <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900" rel="stylesheet">

  <!-- Icon fonts -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/fontawesome.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/ionicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/linearicons.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/open-iconic.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/fonts/pe-icon-7-stroke.css') }}">

  <!-- Core stylesheets -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/bootstrap.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/appwork.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/theme-corporate.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/colors.css') }}"> 
  <link rel="stylesheet" href="{{ asset('assets/vendor/css/uikit.css') }}">
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/growl/growl.css')}}">
  
  <script src="{{ asset('assets/vendor/js/material-ripple.js') }}"></script>
  <script src="{{ asset('assets/vendor/js/layout-helpers.js') }}"></script>
  

  <!-- Core scripts -->
  <script src="{{ asset('assets/vendor/js/pace.js') }}"></script>
  <script src="{{ asset('assets/vendor/js/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/libs/input-mask/inputmask.js')}}"></script>
  <!-- Libs -->
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/datatables/datatables.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/toastr/toastr.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/typeahead-js/typeahead.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-daterangepicker/bootstrap-daterangepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-material-datetimepicker/bootstrap-material-datetimepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/timepicker/timepicker.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/minicolors/minicolors.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-markdown/bootstrap-markdown.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/bootstrap-sweetalert/bootstrap-sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/vendor/libs/sweetalert2/sweetalert2.css')}}"> 
  <link rel="stylesheet" href="{{asset('assets/vendor/libs/spinkit/spinkit.css')}}">

  <!-- App -->
  <link rel="stylesheet" href="{{ asset('assets/css/demo.css') }}">  

  <style>
    .modal-body .table tbody tr td.center {
      text-align: center;
    }

    .modal-icon-plus,
    .modal-icon-minus {
      font-size: 12px;
    }

    .table th,
    .table td {
      vertical-align: middle;
    }

    .modal-icon-plus {
      color: #0a77e8;
    }

    .modal-icon-minus {
      color: #fc5a5c;
    }

    #layout-sidenav {
      /* background-color: #1762ad !important; */
      background-color: #352460 !important;

    }

    .sidenav.bg-blue .sidenav-item.active>.sidenav-link:not(.sidenav-toggle) {
      background-color: #352460 !important;
    }

    .sidenav.bg-blue .sidenav-item.open:not(.sidenav-item-closing)>.sidenav-toggle,
    .sidenav.bg-blue .sidenav-item.active>.sidenav-link {
      color: #fff;
    }

    .sidenav.bg-dark .sidenav-link,
    .sidenav.bg-dark .sidenav-horizontal-prev,
    .sidenav.bg-dark .sidenav-horizontal-next {
      color: #cecece;
      background-color: #352460 !important;
    }

    .bg-darken {
      background-color: #2f1e57 !important; 
      /* // 2f1e57  391e61 */
      color: #cecece !important;

    }

    .bg-darken a {
      color: #fff;
    }

    .app-brand-logo.demo {
      display: -ms-flexbox;
      display: flex;
      width: 45px;
      height: 45px;
      border-radius: 50%;
      -ms-flex-align: center;
      align-items: center;
      -ms-flex-pack: center;
      justify-content: center;
    }

    div.nav-tabs-left>ul>li a.active {
      font-weight: bold;

    }


    .default-style .flatpickr-calendar.open {
      z-index: 10000 !important;

    }

    .invalid {
      visibility: hidden !important;
    }



    .modal-body,
    .modal-footer,
    .modal-header {
      padding: 1.0em !important;
    }


    .bold {
      font-weight: 500;
    }
  </style>
  <link rel="stylesheet" href="{{asset('assets/css/app/cartoes.css')}}">
</head>

<body>
  <div class="page-loader">
    <div class="bg-primary"></div>
  </div>

  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-1 layout-without-sidenav">
    <div class="layout-inner">
      <!-- Layout container -->
      <div class="layout-container">

        <!-- Layout content -->
        <div class="layout-content">

          <!-- Layout sidenav -->
          <div id="layout-sidenav" class="layout-sidenav-horizontal sidenav sidenav-horizontal flex-grow-0 bg-dark container-p-x">

            <!-- Links -->
            <ul class="sidenav-inner">

              <!-- Home -->
              <li class="sidenav-item">
                <a href="{{url('/')}}" class="sidenav-link"><i class="sidenav-icon ion ion-md-speedometer"></i>
                  <div>Home</div>
                </a>
              </li>

              <!-- Configurações -->
              <li class="sidenav-item active">
                <a href="{{url('cliente')}}" class="sidenav-link"><i class="sidenav-icon ion ion-ios-albums"></i>
                  <div>Clientes</div>
                </a>
              </li>
            </ul>
          </div>
          <!-- / Layout sidenav -->

          <!-- Content -->
          <div class="container-fluid flex-grow-1 container-p-y">
            @yield('content')
          </div>
          <!-- / Content -->

          <!-- Layout footer -->
          <nav class="layout-footer footer bg-footer-theme">
            <div class="container-fluid d-flex flex-wrap justify-content-between text-center container-p-x pb-3">
              <div class="pt-3">
                <span class="footer-text font-weight-bolder">Desafio</span> ©
              </div>
              <div>
                <a href="javascript:void(0)" class="footer-link pt-3">Sobre Nós</a>
                <a href="javascript:void(0)" class="footer-link pt-3 ml-4">Contatos</a>
              </div>
            </div>
          </nav>
          <!-- / Layout footer -->

        </div>
        <!-- Layout content -->

      </div>
      <!-- / Layout container -->

    </div>
  </div>
  <!-- / Layout wrapper -->

  <!-- Core scripts -->
  <script src="{{asset('assets/vendor/libs/popper/popper.js')}}"></script>
  <script src="{{asset('assets/vendor/js/bootstrap.js')}}"></script>
  <script src="{{asset('assets/vendor/js/sidenav.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/datatables/datatables.js')}}"></script>
  <!-- Libs -->
  <script src="{{asset('assets/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/moment/moment.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/flatpickr.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/flatpickr/pt.js')}}"></script>

  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/vanilla-text-mask.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vanilla-text-mask/text-mask-addons.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/growl/growl.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/smartwizard/smartwizard.js')}}"></script>
  {{-- <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script> --}}
  <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/default-bootstrap-multiselect.js')}}"></script>
  {{-- <script src="{{asset('assets/vendor/libs/simple-mask-money/simple-mask-money.js')}}"></script> --}}
  <script src="{{asset('assets/vendor/libs/datatables/datatables.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/datatables/dataTables.scroller.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/ekko-lightbox/ekko-lightbox.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vibrantjs/jquery.primarycolor.min.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/vibrantjs/Vibrant.min.js')}}"></script>  
  <script src="{{asset('assets/vendor/libs/block-ui/block-ui.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/sweetalert2/sweetalert2.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/select2/select2.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-select/bootstrap-select.js')}}"></script>
  <script src="{{asset('assets/vendor/libs/bootstrap-multiselect/bootstrap-multiselect.js')}}"></script>
  

  <script src="{{asset('assets/js/app/helper.js')}}"></script>
  <script src="{{ asset('assets/vendor/libs/spin/spin.js') }}"></script>


</body>

</html>