@extends('layouts.app')
@section('content')
<div class="nav-tabs-left mb-4">
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link geral active" data-toggle="tab" href="#navs-left-cliente-cadastro">Cadastro</a>
    </li>
    <li class="nav-item">
      <a class="nav-link geral nav-cliente-listagem" data-toggle="tab" href="#navs-left-cliente-listagem">Listagem</a>
    </li>
    <li class="nav-item d-none nav-item-editar">
      <a class="nav-link geral nav-editar" data-toggle="tab" href="#navs-left-cliente-edicao">Editar</a>
    </li>
  </ul>
  <div class="tab-content">
    {{-- cadastro de clientes --}}
    <div class="tab-pane fade active show box-geral" id="navs-left-cliente-cadastro">
      <div class="card-body">
        @include('cliente/include/cadastro')
      </div>
    </div>
    {{-- listagem de clientes --}}
    <div class="tab-pane fade box-geral" id="navs-left-cliente-listagem">
      <div class="card-body">
        <form id="form-busca-cliente">
            <div class="form-group">
                <div class="input-group">
                <input type="text" class="form-control" name="nome" placeholder="Nome">
                <select name="estado" class="form-control" id="estado">
                    <option value="" disabled selected>Selecione...</option>
                    @foreach ($estados as $est)
                    <option value="{{$est->id}}" data-uf="{{$est->uf}}">{{$est->estado}}</option>   
                    @endforeach
                </select>
                <select name="categoria" class="form-control" id="categoria">
                    <option value="" disabled selected>Selecione...</option>
                    @foreach ($categorias as $cat)
                    <option value="{{$cat->id}}">{{$cat->nome}}</option>   
                    @endforeach
                </select>
                <span class="input-group-append">
                    <button class="btn btn-secondary" type="submit">Buscar</button>
                </span>
                </div>
            </div>
        </form>
        <div id="box-clientes"></div>
      </div>
    </div>
    {{-- clientes edição --}}

    <div class="tab-pane fade navs-left-cliente-edicao box-geral d-none" id="navs-left-cliente-edicao">
      <div class="card-body" id="box-cliente-edicao">
      </div>
    </div>

  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="modalTelefone" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Telefones</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
      <div class="modal-body">
        <div class="container-fluid" id="box-telefones">

        </div>
      </div>

    </div>
  </div>
</div>

<script>
$(".nav-cliente-listagem").click(function(){
  clientes();
})
$("#form-busca-cliente").submit(function(e){
  e.preventDefault();
  let filtro = $(this).serialize()
  clientes(filtro);
})
async function clientes(filtro) {
  $clientes = await $.ajax({
    type: "GET",
    url: "{{url('cliente/clientes')}}",
    data: filtro,
    success: function(data) {
        $('#box-clientes').html(data);
    },
    complete: function () {
      carregaCliente();
    }
  }) 
  return $clientes; 
}
</script>
@endsection