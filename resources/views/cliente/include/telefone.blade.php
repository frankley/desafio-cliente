<table class="table table-striped table-bordered">
    <thead class="thead-inverse">
        <tr>
            <th>Telefone</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($telefones as $tel)
            <tr>
                <td>{{$tel->contato}}</td>
            </tr>
            @endforeach
        </tbody>
</table>