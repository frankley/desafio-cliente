<form id="form-cliente" method="POST">
    @csrf
    <div class="row form-group">
        <div class="col-md">
            <label>Pessoa Fisica ou Juridica</label>
            <input type="text" name="nome" class="form-control" placeholder="Insira Nome PF ou PJ" required>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md">
            <label for="">Estado</label>
            <select name="estado" class="form-control" id="estado" required>
                <option value="" disabled selected>Selecione...</option>
                @foreach ($estados as $est)
                <option value="{{$est->id}}" data-uf="{{$est->uf}}">{{$est->estado}}</option>   
                @endforeach
            </select>
        </div>

        <div class="col-md">
            <label>Tipo Cliente</label>
            <select name="tp_cliente" class="form-control" id="tp_cliente" required>
                <option value="" disabled selected>Selecione...</option>
                <option value="PF">Pessoa Fisica</option>
                <option value="PJ">Pessoa Juridica</option>
            </select>
        </div>
        <div class="col-md">
            <label>CPF ou CNPJ</label>
            <input type="text" name="cpf_cnpj" class="form-control masc_cpf_cnpj" required>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md">
            <label>Data PF ou PJ</label>
            <input type="date" name="data" class="form-control" required>
        </div>
        <div class="col-md">
            <label for="">Categorias</label>
            <select name="categoria" class="form-control" id="categoria" required>
                <option value="" disabled selected>Selecione...</option>
                @foreach ($categorias as $cat)
                <option value="{{$cat->id}}">{{$cat->nome}}</option>   
                @endforeach
            </select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12 input-group" id="add-telefone">
            <input type="hidden" name="count_telefone" id="count-telefone" value="1">
            <div class="input-group-append">
                <button type="button" class="btn btn-success" id="btn-add-telefone">+</button>
            </div>
            <input type="text" name="telefone[]" id="telefone" class="form-control masc-telefone" placeholder="Somente Numeros" required>
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md d-flex justify-content-end">
            <button type="submit" class="btn btn-info">Salvar</button>
        </div>
    </div>
</form>
<script>
$(document).ready(function(){
    masc_telefone();
    masc_cpf_cnpj();
})
function masc_telefone(){
    $(".masc-telefone").inputmask({
      mask: ["(99) 9999-9999", "(99) 99999-9999", ],
      keepStatic: true
    });
}
function masc_cpf_cnpj() {
    $(".masc_cpf_cnpj").inputmask({
      mask: ["999.999.999-99", "99.999.999/9999-99"],
      keepStatic: true
    });
}
$("#btn-add-telefone").click(function(){
    let count = parseInt($("#count-telefone").val());
    count+=1;
    $("#count-telefone").val(count);
    $("#add-telefone").append('<input type="text" name="telefone[]" id="telefone'+count+'" class="form-control masc-telefone" placeholder="Somente Numeros">')
    masc_telefone();
})

$("#estado").change(function(){
    let uf = $(this).find(':selected').data('uf');
    if(uf == 'MG'){
        $('#tp_cliente option').removeAttr('selected').filter('[value=PJ]').attr('selected', true)
        $('#tp_cliente option').filter('[value=PF]').attr('disabled', true);
    }else{
        $('#tp_cliente option').removeAttr('selected').filter('[value=""]').attr('selected', true)
        $('#tp_cliente option').filter('[value=PJ]').attr('disabled', false);
        $('#tp_cliente option').filter('[value=PF]').attr('disabled', false);
    }
})
$("#form-cliente").submit(function(e){
    e.preventDefault();
    $.ajax({
        url: "{{url('cliente/store')}}",
        type: "POST",
        data: $(this).serialize(),
        success: function (data) {
            // console.log(data);
        },
        complete: function(){
            $('#form-cliente').trigger("reset");
            $(".nav-item-editar").addClass('d-none')
            $(".navs-left-cliente-edicao").addClass('d-none');
            growlSuccess('Cadastro Cliente', 'Cliente foi cadastrado com sucesso!')
        }
    })
})
</script>