<!--  Datatables -->
<div class="table-responsive ">
    <table id="datatables-clientes" class="table table-striped">
      <thead class="thead-light">
        <tr>
          <th>CLIENTE</th>        
          <th>CPF/CNPJ</th>
          <th>TIPO CLIENTE</th>
          <th>ESTADO</th>
          <th>CATEGORIA</th>
          <th>TELEFONE</th>
          <th>AÇÕES</th>
        </tr>
      </thead>
      <tbody>
      @foreach ($clientes as $cli)
      <tr>
          <td>{{$cli->nome}}</td>
          <td>{{$cli->cpf_cnpj}}</td>
          <td>{{$cli->tp_cliente == 'PJ' ? 'Pessoa Juridica' : 'Pessoa Fisica'}}</td>
          <td>{{$cli->estado}}</td>
          <td>{{$cli->categoria}}</td>
          <td>
            <button class="btn btn-info btn-sm btn-tele" data-cliente_id="{{$cli->cliente_id}}"><i class="fa fa-eye" aria-hidden="true"></i></button>
          </td>
          <td>
            <button class="btn btn-danger btn-sm btn-delete" data-cliente_id="{{$cli->cliente_id}}"><i class="fas fa-trash" aria-hidden="true"></i></button>
            <button class="btn btn-success btn-sm btn-edit" data-cliente_id="{{$cli->cliente_id}}"><i class="fas fa-edit" aria-hidden="true"></i></button>
          </td>
      </tr> 
      @endforeach
      </tbody>
    </table>
  </div>
  <!-- / Datatables -->
  
  <script>
  function carregaCliente(){
    $.fn.dataTable.ext.errMode = 'throw';
    if ( $.fn.dataTable.isDataTable( '#datatables-clientes' ) ) {
            mainTable.destroy();
    }

    mainTable = $('#datatables-clientes').DataTable({
        language: {
            url: "{{asset('assets/localisation/Portuguese-Brasil.json')}}"    
        },
        dom: 'rtip',
    });
  }

  $('.btn-tele').click(function() {
    let cliente_id = $(this).data('cliente_id')
    $.ajax({
      type: "GET",
      url: "{{url('cliente/telefones')}}",
      data: {
        cliente_id: cliente_id
      },
      success: function(data) {
          $('#box-telefones').html(data);
      },
      complete: function(){
        $('#modalTelefone').modal('show');
      }
    }) 
  })

  $('.btn-delete').click(function() {
    let cliente_id = $(this).data('cliente_id')
    Swal.fire({
        icon: "warning",
        title: 'Excluir Cliente.',
        html: "Deseja excluir o cliente?",
        showCancelButton: true,
        confirmButtonText: 'Sim, Excluir!',
        confirmButtonColor: '#FF8B18',
        cancelButtonText: 'Não'
    }).then((result) => {
        if (result.value) {
          $.ajax({
            type: "GET",
            url: "{{url('cliente/delete')}}",
            data: {
              cliente_id: cliente_id
            },
            success: function(data) {
              clientes();
              growlError('Cliente', data)
            }, 
          }) 
        }
    });      
  })

  $('.btn-edit').click(function() {
    let cliente_id = $(this).data('cliente_id')
    $.ajax({
      type: "GET",
      url: "{{url('cliente/editar')}}",
      data: {
        cliente_id: cliente_id
      },
      success: function(data) {
          $('#box-cliente-edicao').html(data);
          $('.geral').removeClass('active');
          $('.box-geral').removeClass('active show');
          $(".navs-left-cliente-edicao").addClass('active show');
          $(".nav-editar").addClass('active');
          $(".nav-item-editar").removeClass('d-none')
          $(".navs-left-cliente-edicao").removeClass('d-none');
      },
    }) 
  })
  </script>