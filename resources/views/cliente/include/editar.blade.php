<form id="form-cliente-update" method="POST">
    @csrf
    <div class="row form-group">
        <div class="col-md">
            <label>Pessoa Fisica ou Juridica</label>
            <input type="hidden" name="cliente_id" value="{{$cliente->cliente_id}}">
            <input type="text" name="update_nome" class="form-control" placeholder="Insira Nome PF ou PJ" required value="{{$cliente->nome}}">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md">
            <label for="">Estado</label>
            <select name="update_estado" class="form-control" id="update_estado" required>
                <option value="" disabled selected>Selecione...</option>
                @foreach ($estados as $est)
                <option value="{{$est->id}}" data-uf="{{$est->uf}}" @if($est->id == $cliente->estado_id) selected @endif>{{$est->estado}}</option>   
                @endforeach
            </select>
        </div>

        <div class="col-md">
            <label>Tipo Cliente</label>
            <select name="update_tp_cliente" class="form-control" id="update_tp_cliente" required>
                <option value="" disabled selected>Selecione...</option>
                <option value="PF" @if($cliente->tp_cliente == "PF") selected @endif>Pessoa Fisica</option>
                <option value="PJ" @if($cliente->tp_cliente == "PJ") selected @endif>Pessoa Juridica</option>
            </select>
        </div>
        <div class="col-md">
            <label>CPF ou CNPJ</label>
            <input type="text" name="update_cpf_cnpj" class="form-control masc_cpf_cnpj" required value="{{$cliente->cpf_cnpj}}">
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md">
            <label>Data PF ou PJ</label>
            <input type="date" name="update_data" class="form-control" required value="{{$cliente->data}}">
        </div>
        <div class="col-md">
            <label for="">Categorias</label>
            <select name="update_categoria" class="form-control" id="update_categoria" required>
                <option value="" disabled selected>Selecione...</option>
                @foreach ($categorias as $cat)
                <option value="{{$cat->id}}" @if($cat->id == $cliente->categoria_id) selected @endif>{{$cat->nome}}</option>   
                @endforeach
            </select>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-12 input-group" id="update-telefone">
            <input type="hidden" name="update_count_telefone" id="update_count-telefone" value="{{$count_telefones->total}}">
            <div class="input-group-append">
                <button type="button" class="btn btn-success" id="btn-add-telefone-update">+</button>
            </div>
            @foreach ($telefones as $item)
            <input type="text" name="update_telefone[]" id="telefone" value="{{$item->contato}}" class="form-control masc-telefone" placeholder="Somente Numeros" required> 
            @endforeach      
        </div>
    </div>

    <div class="row form-group">
        <div class="col-md d-flex justify-content-end">
            <button type="submit" class="btn btn-info">Atualizar</button>
        </div>
    </div>
</form>
<script>
$(document).ready(function(){
    masc_telefone();
    masc_cpf_cnpj();
})
function masc_telefone(){
    $(".masc-telefone").inputmask({
      mask: ["(99) 9999-9999", "(99) 99999-9999", ],
      keepStatic: true
    });
}
function masc_cpf_cnpj() {
    $(".masc_cpf_cnpj").inputmask({
      mask: ["999.999.999-99", "99.999.999/9999-99"],
      keepStatic: true
    });
}
$("#btn-add-telefone-update").click(function(){
    let count = parseInt($("#update_count-telefone").val());
    count+=1;
    // alert(count);
    $("#update_count-telefone").val(count);
    $("#update-telefone").append('<input type="text" name="update_telefone[]" id="telefone'+count+'" class="form-control masc-telefone" placeholder="Somente Numeros">')
    masc_telefone();
})

$("#update_estado").change(function(){
    let uf = $(this).find(':selected').data('uf');
    if(uf == 'MG'){
        $('#update_tp_cliente option').removeAttr('selected').filter('[value=PJ]').attr('selected', true)
        $('#update_tp_cliente option').filter('[value=PF]').attr('disabled', true);
    }else{
        $('#update_tp_cliente option').removeAttr('selected').filter('[value=""]').attr('selected', true)
        $('#update_tp_cliente option').filter('[value=PJ]').attr('disabled', false);
        $('#update_tp_cliente option').filter('[value=PF]').attr('disabled', false);
    }
})
$("#form-cliente-update").submit(function(e){
    e.preventDefault();
    $.ajax({
        url: "{{url('cliente/atualizar')}}",
        type: "POST",
        data: $(this).serialize(),
        success: function (data) {
            // console.log(data);
        },
        complete: function(){
            // $('#form-cliente').trigger("reset");
            growlNotice('Atualização Cliente', 'Cliente foi atualizado com sucesso!')
        }
    })
})
</script>