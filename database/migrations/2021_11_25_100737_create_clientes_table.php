<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('estado_id');
            $table->foreign('estado_id')
                    ->references('id')->on('estados');
            $table->unsignedBigInteger('categoria_id');
            $table->foreign('categoria_id')
                    ->references('id')->on('categorias');     
            $table->string('nome');
            $table->string('cpf_cnpj', 50);
            $table->enum('tp_cliente', ['PF', 'PJ']);
            $table->date('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
