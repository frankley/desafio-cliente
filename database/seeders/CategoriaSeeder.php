<?php

namespace Database\Seeders;

use App\Models\Categoria;
use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categorias = [
            ['nome' => 'Diamante'],
            ['nome' => 'Ouro'],
            ['nome' => 'Prata'],
            ['nome' => 'Bronze'],
        ];

        foreach ($categorias as $cat) {
            Categoria::create(['nome' => $cat['nome']]);
        }
        
    }
}
