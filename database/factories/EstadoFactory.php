<?php

namespace Database\Factories;

use App\Models\Estado;
use Illuminate\Database\Eloquent\Factories\Factory;


class EstadoFactory extends Factory
{
    protected $model = Estado::class;

    public function withFaker()
    {
        return \Faker\Factory::create('pt_BR');
    }

    public function definition()
    {
        return [
            "estado" => $this->faker->unique()->state,
            "uf" => $this->faker->unique()->stateAbbr 
        ];
    }
}
