<?php

namespace Database\Factories;

use App\Models\Telefone;
use Illuminate\Database\Eloquent\Factories\Factory;

class TelefoneFactory extends Factory
{
    protected $model = Telefone::class;

    public function withFaker()
    {
        return \Faker\Factory::create('pt_BR');
    }

    public function definition()
    {
        return [
            "cliente_id" => rand(1,50),
            "contato" => $this->faker->phoneNumber,
        ];
    }
}
