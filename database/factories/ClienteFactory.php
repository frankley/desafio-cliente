<?php

namespace Database\Factories;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClienteFactory extends Factory
{
    protected $model = Cliente::class;

    public function withFaker()
    {
        return \Faker\Factory::create('pt_BR');
    }

    public function definition()
    {
        return [
            "estado_id" => rand(1,25),
            "categoria_id" => rand(1,4),
            "nome" => $this->faker->name,
            "cpf_cnpj" => $this->faker->unique()->cpf,
            "tp_cliente" => 'PF',
            "data" => $this->faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
        ];
    }
}